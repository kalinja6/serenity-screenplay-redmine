package cz.cvut.fel.still.sqa.redmine.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.NewProjectForm.PROJECT_NAME_FIELD;
import static cz.cvut.fel.still.sqa.redmine.pageobjects.NewProjectForm.SUBMIT;
import static cz.cvut.fel.still.sqa.redmine.pageobjects.ProjectsPage.NEW_PROJECT_LINK;
import static cz.cvut.fel.still.sqa.redmine.pageobjects.TopMenu.PROJECT_PAGE_LINK;
import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class CreateAnProject implements Task {
    private String name;

    public CreateAnProject(String name) {
        this.name = name;
    }

    @Step("{0} creates an project named '#name' with identifier '#identifier'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PROJECT_PAGE_LINK),
                Click.on(NEW_PROJECT_LINK),
                Enter.theValue(name).into(PROJECT_NAME_FIELD),
                Click.on(SUBMIT)
                );
    }

    public static CreateAnProject named(String name) {
        return instrumented(CreateAnProject.class, name);
    }
}
