package cz.cvut.fel.still.sqa.redmine.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.thucydides.core.annotations.Step;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.Notifications.SUCCESS_NOTIFICATION;

/**
 * @author Vaclav Rechtberger
 */
public class SuccessMessage implements Question<String> {
    @Override
    @Step("Notification about successful bug insertion.")
    public String answeredBy(Actor actor) {
        return Text.of(SUCCESS_NOTIFICATION)
                .viewedBy(actor).asString();
    }

    public static Question<String> text() {
        return new SuccessMessage();
    }
}
