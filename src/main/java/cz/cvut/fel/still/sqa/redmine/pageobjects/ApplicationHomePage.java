package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

/**
 * @author Vaclav Rechtberger
 */
@DefaultUrl("http://demo.redmine.org/")
public class ApplicationHomePage extends PageObject {
}