package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class TopMenu extends PageObject {
    public static Target ACTIVE_USER = Target
            .the("Active user link")
            .locatedBy("*.user.active");
    public static final Target REGISTER_PAGE_LINK = Target
            .the("Register link")
            .locatedBy("*.register");
    public static final Target PROJECT_PAGE_LINK = Target
            .the("Projects link")
            .located(By.cssSelector("a.projects"));
}
