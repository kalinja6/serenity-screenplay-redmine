package cz.cvut.fel.still.sqa.redmine.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.RegistrationForm.*;
import static cz.cvut.fel.still.sqa.redmine.pageobjects.TopMenu.REGISTER_PAGE_LINK;
import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class CreateAnAccount implements Task {
    private String login;
    private String password;
    private String passwordConfirmtion;
    private String firstName;
    private String lastName;
    private String email;

    public CreateAnAccount(String login, String password, String passwordConfirmtion, String firstName, String lastName, String email) {
        this.login = login;
        this.password = password;
        this.passwordConfirmtion = passwordConfirmtion;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @Step("{0} creates an account with credentials - name: '#name', password: '#password' and email: '#email'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REGISTER_PAGE_LINK),
                Enter.theValue(login).into(LOGIN_FIELD),
                Enter.theValue(password).into(PASSWORD_FIELD),
                Enter.theValue(passwordConfirmtion).into(PASSWORD_FIELD_CONFIRMATION),
                Enter.theValue(firstName).into(FIRSTNAME_FIELD),
                Enter.theValue(lastName).into(LASTNAME_FIELD),
                Enter.theValue(email).into(MAIL_FIELD),
                Click.on(SUBMIT));
    }

    public static CreateAnAccount withCredentials(String login, String password, String passwordConfirmtion, String firstName, String lastName, String email) {
        return instrumented(CreateAnAccount.class, login, password, passwordConfirmtion, firstName, lastName, email);
    }
}
