package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class HeaderMenu extends PageObject {
    public static Target PROJECTS_SELECT_BOX = Target.the("Select project").located(By.id("project_quick_jump_box"));
}
