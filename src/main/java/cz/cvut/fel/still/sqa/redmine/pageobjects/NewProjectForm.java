package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class NewProjectForm extends PageObject {
    public static Target PROJECT_NAME_FIELD = Target.the("Project name field").located(By.id("project_name"));
    public static Target PROJECT_IDENTIFIER_FIELD = Target.the("Project identifier field").located(By.id("project_identifier"));
    public static Target SUBMIT = Target.the("Submit new project form").locatedBy("//input[@type='submit' and @name='commit']");

}
